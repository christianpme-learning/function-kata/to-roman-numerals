package toromannumberals;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class AppTest 
{
    @Test
    public void parse_1_Test(){
        final App app = new App();
        final String actual = app.parse(1);
        assertTrue("I".equals(actual));
    }

    @Test
    public void parse_2_Test() {
        final App app = new App();
        final String actual = app.parse(2);
        assertTrue("II".equals(actual));
    }

    @Test
    public void parse_4_Test() {
        final App app = new App();
        final String actual = app.parse(4);
        assertTrue("IV".equals(actual));
    }

    @Test
    public void parse_5_Test() {
        final App app = new App();
        final String actual = app.parse(5);
        assertTrue("V".equals(actual));
    }

    @Test
    public void parse_9_Test() {
        final App app = new App();
        final String actual = app.parse(9);
        assertTrue("IX".equals(actual));
    }

    @Test
    public void parse_10_Test() {
        final App app = new App();
        final String actual = app.parse(10);
        assertTrue("X".equals(actual));
    }

    @Test
    public void parse_42_Test() {
        final App app = new App();
        final String actual = app.parse(42);
        assertTrue("XLII".equals(actual));
    }

    @Test
    public void parse_99_Test() {
        final App app = new App();
        final String actual = app.parse(99);
        assertTrue("XCIX".equals(actual));
    }

    @Test
    public void parse_2013_Test() {
        final App app = new App();
        final String actual = app.parse(2013);
        assertTrue("MMXIII".equals(actual));
    }
}
