package toromannumberals;

import java.util.HashMap;
import java.util.Map;

public class App 
{
    public String parse(final Integer number) {
        final StringBuilder numeralsBuilder = new StringBuilder();

        int digits = Integer.toString(number).length();

        for (final Character character : Integer.toString(number).toCharArray()) {

            --digits;
            final Integer digit = Integer.parseInt(character.toString());
            final int factor = (int) Math.pow(10, digits);
            Integer current = digit * factor;
            
            for (final Integer key : keySetSorted()) {
                if (current > 0 && current < key) {
                    final int tryKey = Math.abs(current - key);
                    if (getTranslatorMap().keySet().contains(tryKey)) {
                        numeralsBuilder.append(getTranslatorMap().get(tryKey));
                        current = current - tryKey;

                        if (current > 0) {
                            numeralsBuilder.append(getTranslatorMap().get(key));
                            current = current - key;
                        }
                    }
                } else {
                    while (current > 0) {
                        numeralsBuilder.append(getTranslatorMap().get(key));
                        current = current - key;
                    }
                }
            }
        }
        return numeralsBuilder.toString();
    }

    public Map<Integer, String> getTranslatorMap() {
        final Map<Integer, String> dictionary = new HashMap<>();
        dictionary.put(1000, "M");
        dictionary.put(500, "D");
        dictionary.put(100, "C");
        dictionary.put(50, "L");
        dictionary.put(10, "X");
        dictionary.put(5, "V");
        dictionary.put(1, "I");
        return dictionary;
    }

    public Integer[] keySetSorted(){
        return new Integer[]{1000, 500, 100, 50, 10, 5, 1};
    }
}